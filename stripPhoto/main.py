# This is a Python script to create a strip photo from a video.
import math
import os
import sys
import cv2 as cv
import numpy as np

max_width = 20000


def print_use():
    path, name = os.path.split(sys.argv[0])
    # Use a breakpoint in the code line below to debug your script.
    print('This is a Python script to create a strip photo from a video.')
    print(f'Usage:\n\tpython {name} <source-video-file-name> <result-file-name> [<WIDTHxHEIGHT>]')
    print('If the output file type is a common image type (bmp, jpg, png, tiff) then the result is a still image.')
    print('If the output file type is a video type (avi, mp4) then the result is a video.')
    print('The optional parameter defines resolution for internal processing')


def open_video(src):
    if os.path.exists(src):
        cap = cv.VideoCapture(src)
        width = int(cap.get(cv.CAP_PROP_FRAME_WIDTH)) if cap else 0
        height = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)) if cap else 0
        count = int(cap.get(cv.CAP_PROP_FRAME_COUNT)) if cap else 0
        print(f"Video {width}x{height} {count} frames")
        return cap, count, width, height

    print(f"File {src} doesn't exist")
    return None, 0, 0, 0


def strip_photo(src, dst):
    print(f'\nConversion from {src} to still image {dst}')
    video, count, width, height = open_video(src)
    if video:
        img = np.zeros([height, count, 3], dtype=np.uint8)
        middle = width // 2
        idx = 0
        while True:
            ret, frame = video.read()
            if not ret:
                video.release()
                break
            img[0:height, idx] = frame[0:height, middle]
            if idx % 100 == 0:
                print('.', end='')
            idx += 1

        video.release()
        frame = img

        print(f'\nDone {idx} frames')
        cv.imwrite(dst, frame)
        cv.imshow("image", frame / 255)
        cv.waitKey()


# Prepare the output video writer
def make_video_writer(width, height, fps, dst):
    fourcc = cv.VideoWriter_fourcc(*'mp4v')
    out = cv.VideoWriter(dst, fourcc, fps, (width, height))
    return out


def temp_video_name(prefix, idx):
    return prefix + str(idx) + '.mp4'


def strip_in_slices(video, count, width, height, ratio, skip, dst):
    in_prefix = 'in'
    out_prefix = 'out'
    slices = int(math.ceil(ratio))
    if slices > 100:
        slices = 100
    slice_width = width // slices
    total = slice_width * slices
    if total != width:
        print(f'Warning: using width {total} pixels of {width}.')
    print(f'{slices} slices {count} frames of {slice_width}x{height}.')

    video.set(cv.CAP_PROP_POS_FRAMES, 0)
    fps = video.get(cv.CAP_PROP_FPS)
    parts = []
    for i in range(slices):
        parts.append(make_video_writer(slice_width, height, fps, temp_video_name(in_prefix, i)))

    print(f'Writing {slices} input slices with {count} frames of {slice_width}x{height}...')
    skipped = 0
    for frame_idx in range(count):
        ret, frame = video.read()
        if not ret:
            break

        if skip:
            skipped += 1
            if skip == skipped:
                skipped = 0
            else:
                continue

        if frame_idx % 100 == 0:
            print('+', end='')

        for i in range(slices):
            left = slice_width * i
            right = slice_width + left
            parts[i].write(frame[0:height, left:right])

    for i in range(slices):
        parts[i].release()
    parts.clear()
    video.release()

    for i in range(slices):
        strip_video(temp_video_name(in_prefix, i), temp_video_name(out_prefix, i), False, 0)
        os.remove(temp_video_name(in_prefix, i))

    print(f'Combining {slices} parts...')
    out = None
    for i in range(slices):
        inp, cnt, wd, ht = open_video(temp_video_name(out_prefix, i))
        if not out:
            out = make_video_writer(count, height, fps, dst)
        for frame_idx in range(cnt):
            ret, frame = inp.read()
            if not ret:
                break
            out.write(frame)
        inp.release()
        os.remove(temp_video_name(out_prefix, i))

    out.release()


def strip_video(src, dst, root, skip):
    print(f'\nConversion from {src} to video {dst}')
    video, count, width, height = open_video(src)
    if video:
        maximum_data = 200 * 1920 * 1080
        if root and count > width:
            skip = count // width
            count = width
            print(f'Reduced frame number {count}')
        needed_data = 3 * count * width * height
        mb = needed_data / 1024 / 1024
        print(f'Total data {mb:.1f} MB')
        downscale = False
        if root and needed_data > maximum_data:
            if len(sys.argv) > 3:
                resolution = sys.argv[3]
                w, h = resolution.split('x')
                if w and h:
                    print(f'Internal resolution {w}x{h}')
                    downscale = True
                    width = int(w)
                    height = int(h)
                    if root and count > width:
                        count = width
                        print(f'Reduced frame number {count}')
                    needed_data = 3 * count * width * height
                    mb = needed_data / 1024 / 1024
                    print(f'Reduced data {mb:.1f} MB')

            if not downscale:
                strip_in_slices(video, count, width, height, needed_data / maximum_data, skip, dst)
                return

        fps = video.get(cv.CAP_PROP_FPS)
        out = make_video_writer(count, height, fps, dst)
        if not out:
            return

        # Initialize a numpy array to store output frames
        input_frames = np.zeros((count, height, width, 3), dtype=np.uint8)

        skipped = 0

        # Process each frame
        for frame_idx in range(count):
            ret, frame = video.read()
            if not ret:
                break

            # If the source video is extra long we skip some frames
            if skip:
                skipped += 1
                if skip == skipped:
                    skipped = 0
                else:
                    continue

            # Store input frames in 4d shape
            input_frames[frame_idx] = cv.resize(frame, (width, height)) if downscale else frame
            if frame_idx % 100 == 0:
                print('.', end='')

        # transform
        print(f'\nTranspose from {input_frames.shape} to .. ', end='')
        output_frames = input_frames.transpose(2, 1, 0, 3)
        print(f'{output_frames.shape} done\n')
        del input_frames

        # Write output frames to the output video
        for i in range(width):
            out.write(output_frames[i])
            if i % 100 == 0:
                print('*', end='')

        del output_frames        # Release resources
        video.release()
        out.release()


def view_video(src):
    print(f'Viewing {src}')
    video, count, width, height = open_video(src)
    if video:
        while True:
            ret, frame = video.read()
            if not ret:
                break
            cv.imshow("image", cv.resize(frame, (800, 600)) / 255)
            c = cv.waitKey(1) & 0xFF
            if c == ord('q'):
                break
        video.release()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print_use()
    elif len(sys.argv) < 3:
        source = sys.argv[1]
        view_video(source)
    else:
        source = sys.argv[1]
        destination = sys.argv[2]
        path, extension = os.path.splitext(destination)

        if extension.lower() in ('.bmp', '.jpg', '.jpeg', '.png', '.tif', '.tiff'):
            strip_photo(source, destination)
        elif extension.lower() in ('.avi', '.mp4'):
            strip_video(source, destination, True, 0)
        else:
            print(f'Error: Not supported file type {extension}\n')
            print_use()

