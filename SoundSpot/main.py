import cv2 as cv
import rtmidi
import random
from rtmidi.midiconstants import NOTE_OFF, NOTE_ON, ALL_NOTES_OFF, CONTROL_CHANGE, PROGRAM_CHANGE, \
    BANK_SELECT_LSB, BANK_SELECT_MSB, TIMING_CLOCK, POLY_AFTERTOUCH, \
    REGISTERED_PARAMETER_NUMBER_LSB, REGISTERED_PARAMETER_NUMBER_MSB, \
    PITCH_BEND


def loop():
    image = cv.imread('image.jpg')
    h, w, c = image.shape
#    notes = [60, 64, 67]
    notes = [57, 60, 64]

    while True:
        x = random.randrange(0, w)
        y = random.randrange(0, h)
        k = image[y, x]
        print(k)
        for i in range(3):
            note_on(1, notes[i], k[i] // 2)
        cv.rectangle(image, (x-1, y-1), (x+1, y+1), (0, 0, 0))
        cv.imshow('Sound Spot', image)

        # waits for user to press any key
        # (this is necessary to avoid Python kernel form crashing)
        key = cv.waitKey(3000)
        for i in range(3):
            note_off(1, notes[i])

        if key == ord('q') or key == 27:
            break

    cv.destroyAllWindows()


def list_midi():
    midi_out = rtmidi.MidiOut()
    available_ports = midi_out.get_ports()
    del midi_out
    return available_ports


def note_on(instrument, pitch, velocity) -> str:
    if velocity <= 0 or pitch <= 0:
        return f'ON ignored: pitch={pitch} velocity={velocity:.1f}'
    if velocity > 127:
        velocity = 127
    instrument -= 1
    on = [NOTE_ON + instrument, pitch, velocity]
    try:
        midi_out.send_message(on)
        print('NOTE ON Channel ' + str(instrument + 1) + ' Pitch ' + str(pitch) + ' Velocity ' + str(velocity))
        return f'+ON ch={instrument} p={pitch} v={velocity:.1f}'
    except rtmidi.RtMidiError as e:
        print('NOTE ON Channel ' + str(instrument + 1) + ' Pitch ' + str(pitch) + ' Velocity ' + str(velocity) + ' ERR ' + str(e))
        return 'ON fail ' + str(e)
    except OverflowError as o:
        print('NOTE ON Channel ' + str(instrument + 1) + ' Pitch ' + str(pitch) + ' Velocity ' + str(velocity) + ' ERR ' + str(o))
        return 'ON fail ' + str(o)


def note_off(instrument, pitch) -> str:
    instrument -= 1
    off = [NOTE_OFF + instrument, pitch, 0]
    try:
        midi_out.send_message(off)
        print('NOTE OFF Channel ' + str(instrument + 1) + ' Pitch ' + str(pitch))
        return f'+OFF ch={instrument} p={pitch}'
    except rtmidi.RtMidiError as e:
        print('NOTE OFF Channel ' + str(instrument + 1) + ' Pitch ' + str(pitch) + ' ERR ' + str(e))
        return 'OFF fail ' + str(e)
    except OverflowError as o:
        print('NOTE ON Channel ' + str(instrument + 1) + ' Pitch ' + str(pitch) + ' ERR ' + str(o))
        return 'OFF fail ' + str(o)


def all_notes_off():
    message = [CONTROL_CHANGE, ALL_NOTES_OFF, 0]
    try:
        midi_out.send_message(message)
        print('MIDI To all ' + ' '.join(hex(m) for m in message))
    except rtmidi.RtMidiError as e:
        print(str(e))


if __name__ == '__main__':
    ports = list_midi()
    print(ports)
    if len(ports) != 0:

        m = int(input('MIDI [0..' + str(len(ports) - 1) + '] '))

        midi_out = rtmidi.MidiOut()
        try:
            midi_out.open_port(m)
            print(f'opened: {ports[m]}')
        except rtmidi.RtMidiError as e:
            msg = 'failed to open ' + str(ports[p]) + str(e)
            print(msg)

        loop()

        all_notes_off()

