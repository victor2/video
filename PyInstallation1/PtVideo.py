import cv2
import configparser

import MidiCenter

midiCtrl = ''
midiNotes = ''
videoFile = ''
resSize = (640, 480)
channels = dict()
params = dict()


def initParams():
    params['mix1'] = 0.1
    params['mix2'] = 0.5
    params['mix3'] = 0.1
    params['shiftX1'] = 20
    params['shiftY1'] = 10
    params['shiftX2'] = 30
    params['shiftY2'] = 10
    params['shiftX3'] = 20
    params['shiftY3'] = 10
    params['sense1'] = 1
    params['sense2'] = 5
    params['sense3'] = 1


def readConfig():
    global videoFile
    global resSize
    global midiCtrl
    global midiNotes

    config = configparser.ConfigParser()
    config.read('PtVideo.ini')

    midiCtrl = config['MIDI']['control']
    midiNotes = config['MIDI']['notes']
    videoFile = config['Content']['video']
    resSize = (config['Display'].getint('width'), config['Display'].getint('height'))
    channels[config['MIDI'].getint('mix1')] = 'mix1'
    channels[config['MIDI'].getint('mix2')] = 'mix2'
    channels[config['MIDI'].getint('mix3')] = 'mix3'
    channels[config['MIDI'].getint('shiftX1')] = 'shiftX1'
    channels[config['MIDI'].getint('shiftX2')] = 'shiftX2'
    channels[config['MIDI'].getint('shiftX3')] = 'shiftX3'
    channels[config['MIDI'].getint('shiftY1')] = 'shiftY1'
    channels[config['MIDI'].getint('shiftY2')] = 'shiftY2'
    channels[config['MIDI'].getint('shiftY3')] = 'shiftY3'
    channels[config['MIDI'].getint('sense1')] = 'sense1'
    channels[config['MIDI'].getint('sense2')] = 'sense2'
    channels[config['MIDI'].getint('sense3')] = 'sense3'
    print(channels)


def openVideo():
    global videoFile
    vc = cv2.VideoCapture(0)
    vc2 = cv2.VideoCapture(1)
    # Check success
    if not vc.isOpened():
        raise Exception("Could not open video device")

    if not vc2.isOpened():
        print("Only one camera found, using " + videoFile)
        vc2 = cv2.VideoCapture(videoFile)

    return vc, vc2


def mainLoop():
    first = True
    velo = 0

    vc, vc2 = openVideo()
    has2 = vc2.isOpened()

    font = cv2.FONT_HERSHEY_SIMPLEX
    cornerOfText = (10, 10)
    fontScale = 0.4
    fontColor = (255, 255, 255)
    lineType = 1

    control = MidiCenter.getMidiInput(midiCtrl)
    if control is None:
        print('Could not open ' + midiCtrl)

    notes = MidiCenter.getMidiInput(midiNotes)
    if notes is None:
        print('Could not open ' + midiNotes)

    cv2.namedWindow('Python Art Video', cv2.WINDOW_NORMAL)

    while True:
        # Capture frame-by-frame
        ret, frame = vc.read()

        if has2:
            ret, frame2 = vc2.read()
            if not ret:
                has2 = False
                print('End of video 2')

        # Our operations on the frame come here
        # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        mix = params['mix1'] / 500

        if first:
            ht = frame.shape[0]
            wd = frame.shape[1]
            result = frame
            first = False
            print(frame.shape)
            if has2:
                frame2 = cv2.resize(frame2, (wd, ht));
                print(frame2.shape)
        else:
            result = cv2.addWeighted(result, 1 - mix, frame, mix, 0, -1)

        for i in range(3):
            sX = params['shiftX' + str(i + 1)] // 10
            sY = params['shiftY' + str(i + 1)] // 10
            if i == 2:
                result[0: ht - sX, 0: wd - sY, 2] = result[sX: ht, sY: wd, 2]
            else:
                result[sY: ht, sX: wd, 0] = result[0: ht - sY, 0: wd - sX, 0]

        if has2 and frame2 is not None:
            frame2 = cv2.resize(frame2, (wd, ht));
            mix = params['mix2'] / 500
            frame2 = cv2.scaleAdd(frame2, mix, cv2.flip(frame2, -1))
            result = cv2.addWeighted(result, 1 - mix, frame, mix, 0, -1)
            mix = params['mix3'] / 500
            result = cv2.addWeighted(result, 1 - mix, frame2, mix, 0, -1, dtype=cv2.CV_16U)

        # print(velo, params['sense1'], velo * params['sense1'], velo * params['sense1'] // 128)
        for i in range(3):
            result[:, :, i] += velo * params['sense' + str(i + 1)] // 128

        result = cv2.normalize(result, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

        withText = cv2.resize(result, resSize)
        # Write some Text
        cv2.putText(withText, "Press 'q' to exit",
                    cornerOfText,
                    font,
                    fontScale,
                    fontColor,
                    lineType)

        # Display the resulting frame
        cv2.imshow('Python Art Video', withText)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        if control is not None:
            for msg in control.iter_pending():
                if msg.type == 'control_change' and msg.control in channels:
                    target = channels[msg.control]
                    # print(target, msg.value)
                    params[target] = msg.value
                else:
                    print(msg)

        if notes is not None:
            for msg in notes.iter_pending():
                if msg.type == 'note_on':
                    velo = msg.velocity
                elif msg.type == 'note_off':
                    velo = 0

    # When everything done, release the capture
    vc.release()
    if has2:
        vc2.release()

    if control is not None:
        MidiCenter.closeMidiInput(control)
    if notes is not None:
        MidiCenter.closeMidiInput(notes)

    cv2.destroyAllWindows()


if __name__ == "__main__":
    initParams()
    readConfig()
    mainLoop()
