import cv2
import configparser
import math
import numpy as np
import time

frame_time = 0.0


def put_text(image, text, scale=0.45, corner=(10, 10), line=1, color=(0, 127, 127)):
    font = cv2.FONT_HERSHEY_SIMPLEX
    corner_of_text = corner
    font_scale = scale
    font_color = color
    line_type = line

    cv2.putText(image, text,
                (corner_of_text[0] + 1, corner_of_text[1] + 1),
                font,
                font_scale,
                (200, 0, 0),
                line_type)


def get_fps() -> str:
    global frame_time
    t = time.time()
    fps = ''
    if frame_time != 0:
        diff = t - frame_time
        if diff != 0:
            fps = str(int(1 / diff)) + ' fps'
    frame_time = t
    return fps


def read_config() -> None:
    config = configparser.ConfigParser()
    config.read('Greetings.ini')


def start_writing(width, height, name='output.mp4', rate=40.0):
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    return cv2.VideoWriter(name, fourcc, rate, (width, height))


def main_loop() -> None:
    vc = cv2.VideoCapture(0)
    avi = cv2.VideoCapture('video.mp4')
    cv2.namedWindow('Video Greetings', cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("Video Greetings", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    first = True
    mix = 0.1
    mix2 = 0.1
    radius = 5
    theta = np.deg2rad(1)
    sinus = math.sin(theta)
    cosines = math.cos(theta)
    rot = np.array([[cosines, -sinus], [sinus, cosines]])
    v = np.array([radius, 0])
    writing = False

    while True:
        # Capture frame-by-frame
        ret, frame = vc.read()

        if not ret:
            break

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = cv2.flip(frame, 1)

        if first:
            ht = frame.shape[0]
            wd = frame.shape[1]
            result = frame
            first = False
            print(frame.shape)
        else:
            result = cv2.addWeighted(result, 1 - mix, frame, mix, 0, -1)

        if avi.isOpened():
            ret, video = avi.read()
            if ret:
                video = cv2.resize(video, (wd, ht))
                video = cv2.cvtColor(video, cv2.COLOR_BGR2GRAY)
                # result = cv2.addWeighted(result, 1 - mix2, video, mix2, 0, -1)
            else:
                avi.set(cv2.CAP_PROP_POS_FRAMES, 0)
                continue

        v = np.dot(rot, v)
        sx = int(v[0])
        sy = int(v[1])
        # result[0: ht - sx, 0: wd - sy, 2] = result[sx: ht, sy: wd, 2]
        if sy > 0:
            if sx > 0:
                result[0: ht - sx, 0: wd - sy] = result[sx: ht, sy: wd]
            else:
                result[-sx: ht, sy: wd] = result[0: ht + sx, 0: wd - sy]
        else:
            if sx > 0:
                result[0: ht - sx, 0: wd + sy] = result[sx: ht, -sy: wd]
            else:
                result[-sx: ht, -sy: wd] = result[0: ht + sx, 0: wd + sy]

        # Display the resulting frame
        final = cv2.addWeighted(result, 1 - mix2, video, mix2, 0, -1)

        if writing:
            out.write(cv2.resize(cv2.cvtColor(final, cv2.COLOR_GRAY2BGR), (wd, ht)))
            put_text(final, 'REC ' + get_fps())
        else:
            put_text(final, get_fps())

        cv2.imshow('Video Greetings', final)
        result = cv2.normalize(result, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

        c = cv2.waitKey(1) & 0xFF

        if c == ord('q'):
            break

        if c == ord('w'):
            if writing:
                out.release()
            else:
                out = start_writing(wd, ht)
                if not out:
                    writing = True  # will be reverted on the following line

            writing = not writing  # toggle the flag

    # When everything done, release the capture
    vc.release()

    cv2.destroyAllWindows()


if __name__ == "__main__":
    read_config()
    main_loop()
