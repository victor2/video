import mido

def getMidiInput(name):
    midiIns = mido.get_input_names()
    if not name in midiIns:
        print(midiIns)
        for n in midiIns:
            if name in n:
                print(n)
                return mido.open_input(n)

    return None

def closeMidiInput(inp):
    inp.close()



def main():
    inport = getMidiInput('nano')
    
    while True:    
        for msg in inport.iter_pending():
            print(msg)
            
        if input('press q to exit ') == 'q':
            break

    inport.close()

if __name__ == "__main__":
    main()
