import pyaudio    # pip install pyaudio
import wave
import os
import audioop

wf = None
level = 0
looped = True
stopped = False


def callback(in_data, frame_count, time_info, status):
    global level
    global stopped
    data = wf.readframes(frame_count)
    if len(data) != wf.getsampwidth() * wf.getnchannels() * frame_count:
        wf.rewind()
        data = wf.readframes(frame_count)
        if not looped:
            stopped = True
    level = audioop.rms(data, wf.getsampwidth())
#    print(level)
    return data, pyaudio.paContinue


class AudioPlayer:
    au = None
    stream = None

    def __init__(self, audio_file, loop):
        self.au = pyaudio.PyAudio()
        global looped
        looped = loop
        global wf
        if os.path.isfile(audio_file):
            wf = wave.open(audio_file, 'rb')
            self.stream = self.au.open(format=self.au.get_format_from_width(wf.getsampwidth()),
                        channels=wf.getnchannels(),
                        rate=wf.getframerate(),
                        output=True,
                        stream_callback=callback)

            self.stream.start_stream()

    def close(self):
        if self.stream:
            self.stream.stop_stream()
            self.stream.close()
            self.au.terminate()
        if wf:
            wf.close()

    @staticmethod
    def get_level():
        return level

    @staticmethod
    def get_stopped():
        return stopped

    def stop(self):
        self.close()
