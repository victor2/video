import time
import Transforms

automations = []


class Automation:
    pattern = 'triangle' # square
    change = 0.5
    period = 12000
    param = 'mix'
    limits = (0, 120)
    start_time = time.time()

    def __init__(self, param):
        self.param = param
        self.start_time = time.time()

    def get_value(self):
        t = time.time()
        period = self.period / 1000
        delta = t - self.start_time
        if delta > period:
            self.start_time = t
            delta = 0

        current = delta / period
        before = self.change > current
        if self.pattern == 'square':
            if before:
                return self.limits[1]
            else:
                return self.limits[0]

        if self.pattern == 'triangle':
            if before:
                start = self.limits[0]
                end = self.limits[1]
                pos = current / self.change
            else:
                start = self.limits[1]
                end = self.limits[0]
                pos = (current - self.change) / (1 - self.change)

            return int(start + (end - start) * pos)


def add_automation_rule(automation):
    pars = automation.split(',')
    pp = dict()
    for p in pars:
        k, v = p.split(':')
        pp[k] = v

    a = Automation(pp.get('param', 'mix'))
    a.pattern = pp.get('pattern', 'square')
    a.period = int(pp.get('period', '10000'))
    a.change = float(pp.get('change', '0.5'))
    limits = pp.get('limits', '40-120').split('-')
    if len(limits) == 2:
        a.limits = int(limits[0]), int(limits[1])
    automations.append(a)


def automate():
    for a in automations:
        val = a.get_value()
        if type(val) is int:
            Transforms.params[a.param] = val
#            print(val)
