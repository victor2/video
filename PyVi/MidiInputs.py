import mido
import configparser

channels = dict()
midi_ctrl = ''
midi_notes = ''


def load_config(conf):
    global midi_ctrl
    global midi_notes

    config = configparser.ConfigParser()
    config.read(conf)
    vc = int(config['General']['channels'])

    print(mido.get_input_names())

    midi_ctrl = get_midi_input(config['MIDI']['control'])
    midi_notes = get_midi_input(config['MIDI']['notes'])

    variables = {'mix', 'shiftX', 'shiftY', 'sense', 'tileX', 'tileY', 'mirrorX', 'mirrorY', 'saturation', 'rotate', 'trail' }
    variables_on_off = {'flipX', 'flipY', 'mirrors'}

    # making mapping between MIDI channels and parameters
    for p in range(vc):
        for v in variables:
            key = v + str(p + 1)
            if key in config['MIDI']:
                channels[config['MIDI'][key]] = key

        # these parameters can be linked to a second pair of MIDI control/channel
        if 'emix'+str(p + 1) in config['MIDI']:
            channels[config['MIDI']['emix'+str(p + 1)]] = 'mix'+str(p + 1)
        if 'esense' + str(p + 1) in config['MIDI']:
            channels[config['MIDI']['esense' + str(p + 1)]] = 'sense' + str(p + 1)

        for v in variables_on_off:
            key = v + str(p + 1) + 'on'
            if key in config['MIDI']:
                channels[config['MIDI'][key]] = key
            key = v + str(p + 1) + 'off'
            if key in config['MIDI']:
                channels[config['MIDI'][key]] = key
 #    print(channels)


def get_midi_input(name):
    for n in mido.get_input_names():
        if name in n:
            print(n)
            return mido.open_input(n)


def handle_midi(params):
    info = ''
    if midi_ctrl is not None:
        for msg in midi_ctrl.iter_pending():
            if msg.type == 'control_change':
                name = str(msg.control) + ',' + str(msg.channel)
                if name in channels:
                    target = channels[name]
                    print(target, msg.value)
                    if target.endswith('off'):
                        params[target[:-3]] = False
                        info = target[:-3] + ' OFF'
                    elif target.endswith('on'):
                        params[target[:-2]] = True
                        info = target[:-2] + ' ON'
                    else:
                        params[target] = msg.value
                        info = target + ' ' + str(params[target])
                else:
                    print(msg)
                    info = str(msg)
            else:
                print(msg)
                info = str(msg)

    if midi_notes is not None:
        for msg in midi_notes.iter_pending():
            print(msg)
            if msg.type == 'note_on':
                params['velo'] = msg.velocity
                info += ' note_on'
            elif msg.type == 'note_off':
                params['velo'] = 0

    return info


def cleanup():
    if midi_ctrl is not None:
        midi_ctrl.close()
    if midi_notes is not None:
        midi_notes.close()
