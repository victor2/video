import cv2

NUMBER_OF_FRAMES = 80
RESOLUTION_FHD = (1920, 1080)
RESOLUTION_UHD = (3840, 2160)

if __name__ == "__main__":
    v = cv2.VideoCapture('video.mp4')
    if v.isOpened():
        for i in range(NUMBER_OF_FRAMES):
            ret, frame = v.read()
            if ret:
                fhd = cv2.resize(frame, RESOLUTION_FHD)
                uhd = cv2.resize(frame, RESOLUTION_UHD)
                cv2.imwrite('fhd_video' + str(i) + '.bmp', fhd)
                cv2.imwrite('uhd_video' + str(i) + '.bmp', uhd)
