import cv2
import configparser
import MainLoop
import MotionDetector
import Automation
import AudioPlayer

VELO_DEFAULT_RULE = 'shift, brightness'
PROCESS_DEFAULT = 'flip, saturation, velo, brightness, mirror, rgb, shift, tiles'

params = dict()
buffers = []

# currently selected
channel = 1

video_channels = 4

# TODO better design is needed to apply image shift based on velocity.
# This value is set in velo() and is used in affine() which is always execyted last
velo_shift = 0


def mix(stream):
    key = 'mix' + str(stream)
    if key in params:
        return params[key] / 120
    return 0


def init_params(videos):
    global video_channels
    video_channels = len(videos)
    params['wd'] = 800
    params['ht'] = 600

    for p in range(video_channels):
        v = videos[p]
        text = v[1] == 'TXT'
        tile = 10
        trail = 62
        mx = 30
        if text:
            tile = 0
            trail = 65
        params['prc'+str(p + 1)] = PROCESS_DEFAULT
        params['pause'+str(p + 1)] = False
        params['mix'+str(p + 1)] = mx
        params['shiftX'+str(p + 1)] = 65
        params['shiftY'+str(p + 1)] = 67
        params['sense'+str(p + 1)] = 64
        params['flipX'+str(p + 1)] = False
        params['flipY'+str(p + 1)] = False
        params['mirrors'+str(p + 1)] = False
        params['mirrorX'+str(p + 1)] = 70
        params['mirrorY'+str(p + 1)] = 60
        params['rotate'+str(p + 1)] = 64
        params['trail'+str(p + 1)] = trail
        params['tileX'+str(p + 1)] = tile
        params['tileY'+str(p + 1)] = tile
        params['saturation'+str(p + 1)] = 64
        params['velo_rules'+str(p + 1)] = VELO_DEFAULT_RULE

    params['velo'] = 0


def load_config(conf, videos):
    config = configparser.ConfigParser()
    config.read(conf)
    init_params(videos)

    params['wd'] = config['Display'].getint('wd', fallback=400)
    params['ht'] = config['Display'].getint('ht', fallback=300)
    for v in range(video_channels):
        k = 'prc' + str(v + 1)
        if 'General' in config and k in config['General']:
            params[k] = config['General'][k]

    if 'Automation' in config and 'rules' in config['Automation']:
        rules = config['Automation']['rules']
        for rule in rules.split(','):
            if rule in config['Automation']:
                Automation.add_automation_rule(config['Automation'][rule])
            else:
                print('Not found automation rule ' + rule)


def save_params(conf):
    config = configparser.ConfigParser()
    config['values'] = {}
    exclude = ['wd', 'ht', 'velo', 'rules']
    for p in params:
        if p not in exclude:
            config['values'][p] = str(params[p])

    with open(conf, 'w') as configfile:
        config.write(configfile)


def load_params(conf):
    config = configparser.ConfigParser()
    config.read(conf)
    if 'values' not in config:
        print('Parameters not loaded from ', conf)
        return
    
    for p in params:
        # default value
        v = params[p]
        vals = config['values']
        if type(v) is bool:
            params[p] = vals.getboolean(p, fallback=v)
        elif type(v) is int:
            params[p] = vals.getint(p, fallback=v)
        elif type(v) is float:
            params[p] = vals.getfloat(p, fallback=v)
        elif type(v) is str or type(v) is list:
            params[p] = vals.get(p, fallback=v)
        else:
            print('unknown type ', type(v), ' of ', p)


def saturation(stream, frame):
    return _saturation(params['saturation'+str(stream)] // 64, frame)


def _saturation(v, frame):
    s = frame.shape
    if len(s) < 3:
        return frame
    c = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    c[:, :, 1] *= v
    return cv2.cvtColor(c, cv2.COLOR_HSV2BGR)


def flip(stream, frame):
    code = 0
    x = params['flipX'+str(stream)]
    y = params['flipY'+str(stream)]
    if x and y:
        code = -1
    elif x:
        code = 1
    elif y:
        code = 0
    else:
        return frame
    return cv2.flip(frame, code)


def mirror_x(stream, frame):
    wd = frame.shape[1]
    split = params['mirrorX'+str(stream)] * wd // 127
    if split == 0 or not params['mirrors'+str(stream)]:
        return frame
    if split > wd // 2:
        edge = split * 2 - wd
        half = frame[:, edge: split, ]
        frame[:, split: wd, ] = cv2.flip(half, 1)
    else:
        frame[:, split * 2: wd, ] = frame[:, 0: wd - split * 2, ]
        half = frame[:, 0: split, ]
        frame[:, split: split * 2, ] = cv2.flip(half, 1)
    return frame


def mirror_y(stream, frame):
    ht = frame.shape[0]
    split = params['mirrorY'+str(stream)] * ht // 127
    if split == 0 or not params['mirrors'+str(stream)]:
        return frame
    if split > ht // 2:
        edge = split * 2 - ht
        half = frame[edge: split, :, ]
        frame[split: ht, :, ] = cv2.flip(half, 0)
    else:
        frame[split * 2: ht, :, ] = frame[0: ht - split * 2, :, ]
        half = frame[0: split, :, ]
        frame[split: split * 2, :, ] = cv2.flip(half, 0)
    return frame


def rgb(stream, frame):
    # TODO
    return frame


def affine(stream, frame):
    wd = frame.shape[1]
    ht = frame.shape[0]
    angle = params['rotate' + str(stream)] - 64
    trail = params['trail' + str(stream)] / 64
    shift_x = (params['shiftX' + str(stream)] - 64) / 5
    shift_y = (params['shiftY' + str(stream)] - 64) / 5
    cx = params['wd'] // 2
    cy = params['ht'] // 2
    m = cv2.getRotationMatrix2D((cx, cy), angle, trail)
    m[0, 2] += shift_x
    m[1, 2] += shift_y + velo_shift
#    print(m)
    return cv2.warpAffine(frame, m, (wd, ht))


def velo(stream, frame):
    global velo_shift
#    frame[:, :, i] += params['velo'] * params['sense' + str(i + 1)] // 128
    v = params['velo']
    a = AudioPlayer.AudioPlayer.get_level() // 100   # TODO make it configurable
    if v == 0:
        if a == 0:
            return frame
        else:
            v = a

    rules = params['velo_rules'+str(stream)].split(',')
    s = params['sense'+str(stream)] - 64
    v *= s
    v //= 64
    for r in rules:
        r = r.strip()
        if r == 'brightness':
            if v > 0:
                frame += v
            elif v < 0:
                frame -= -v
        elif r == 'saturation':
            frame = _saturation(v, frame)
        elif r == 'shift':
            velo_shift = v
    return frame


def tiles(stream, frame):
    nx = params['tileX' + str(stream)] // 5
    ny = params['tileY' + str(stream)] // 5
    if nx == 0:
        nx = 1
    if ny == 0:
        ny = 1
    ix = frame.shape[1] // nx
    iy = frame.shape[0] // ny
    small = cv2.resize(frame, (ix, iy))
    for x in range(int(nx)):
        x1 = x * ix
        x2 = (x + 1) * ix
        for y in range(int(ny)):
            y1 = y * iy
            y2 = (y + 1) * iy
            frame[y1: y2, x1: x2, ] = small
    return frame


def _process(stream, frame, prc):
    global velo_shift
    velo_shift = 0

    for p in prc.split(','):
        p = p.strip()
        if p == 'flip':
            frame = flip(stream, frame)
        elif p == 'mirror':
            frame = mirror_x(stream, frame)
            frame = mirror_y(stream, frame)
        elif p == 'rgb':
            frame = rgb(stream, frame)
        elif p == 'velo':
            frame = velo(stream, frame)
        elif p == 'tiles':
            frame = tiles(stream, frame)
        elif p == 'saturation':
            frame = saturation(stream, frame)
    return frame


def process_frame(stream, frame, source):
    wd = params['wd']
    ht = params['ht']
    prc = params['prc' + str(stream)]
    frame = cv2.resize(frame, (wd, ht))

    detected = None
    if source == 'CAM':
        detected = MotionDetector.detect_motion(stream, frame)

    if detected is not None:
        MainLoop.put_text(detected, 'MOTION DETECTED', corner=(wd // 2, ht - 50), color=(0, 0, 220))
        frame = detected
    else:
        frame = _process(stream, frame, prc)

    mx = mix(stream)

    frame = cv2.resize(frame, (wd, ht))
    if len(buffers) < stream:
        buffers.append(frame)
    else:
        frame = cv2.addWeighted(buffers[stream - 1], 1 - mx, frame, mx, 0, -1)

    buffers[stream - 1] = affine(stream, frame)
    return frame


# applied to current channel
def handle_key(key):
    global channel
    ch = str(channel)
    MainLoop.info(str(key))
    if key in range(ord('1'), ord('1') + video_channels):
        channel = key - ord('0')
        MainLoop.info("Channel " + str(channel))
    elif key == ord('f'):
        params['flipX'+ch] = not params['flipX'+ch]
        params['flipY'+ch] = not params['flipY'+ch]
    elif key == ord('m'):
        params['mirrors'+ch] = not params['mirrors'+ch]
