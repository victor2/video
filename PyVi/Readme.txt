WINDOWS

1. Install Python 3.6 or later
2. Install libraries:
pip install opencv-python
pip install mido
pip install python-rtmidi
pip install pipwin
pipwin install pyaudio
3. Run MainLoop.py

MACOS

1. Install Python 3.6 or later
2. Install libraries:
pip install opencv-python
pip install mido
pip install audioop-lts
3. Run MainLoop.py
