import cv2
import numpy

# See the article:
# https://levelup.gitconnected.com/build-your-own-motion-detector-using-webcam-and-opencv-in-python-ff5bdb78a55e

references = []
limit = 1000


def detect_motion(stream, frame):
#    if limit == 0:
        # disabled
#        return None

    if len(references) < stream:
        # save reference frame for motion detection
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)
        references.append(gray)
#        cv2.imwrite('reference' + str(stream) + '.jpg', gray)
    else:
        # do the motion detection
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)
        delta_frame = cv2.absdiff(references[stream - 1], gray)
        thresh_frame = cv2.threshold(delta_frame, 30, 255, cv2.THRESH_BINARY)[1]
        total = numpy.count_nonzero(thresh_frame)
        if total > limit:
            references[stream - 1] = gray
            return cv2.cvtColor(thresh_frame, cv2.COLOR_GRAY2BGR)
