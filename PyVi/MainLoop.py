import configparser
import cv2
import numpy as np
import time
import sys
import Transforms
import MidiInputs
import Automation
import AudioPlayer

CONFIG_FILE = 'PyVi.conf'
PARAMS_FILE = 'PyViParams.conf'
# This is not a real constant. It can be overriden in configuration file    
PERSISTENT_PARAMS = True
SONG_MODE = False
TIME_LAPSE = 50  # set 0 to disable

INFO_TEXT = 'press "q" to quit'

show_info = False
writing = False
writing2 = False
time_out = None
message = ''
message_time = 0
frame_time = 0
text_buffer = None


# video inputs - cameras, mpeg4 files
videos = []
# static images
jpegs = []
jpeg_idx = 0

# text to show
texts = []
text_idx = 0

# other parameters
pars = dict()

# Triggers to control jpegs and texts
jpeg_trigger = None
text_trigger = None


class Trigger:
    next_time = 0
    counter = 0
    param = 1

    def __init__(self, rule):
        self.rule, p = rule.split(',')
        self.param = float(p)

    def count(self):
        if 'note' in self.rule:
            self.counter += 1

    # the method returns True if trigger condition happens
    def is_triggered(self):
        t = time.time()
        yes = False
        if 'time' in self.rule:
            yes = t >= self.next_time
        elif 'note' in self.rule:
            yes = self.counter >= self.param

        if yes:
            self.next_time = t + self.param / 1000
            self.counter = 0
        return yes


def info(text):
    global message
    global message_time
    message = text
    message_time = int(time.time()) + 2


def load_config(conf):
    global PERSISTENT_PARAMS
    global jpeg_trigger
    global text_trigger

    config = configparser.ConfigParser()
    config.read(conf)

    PERSISTENT_PARAMS = config['General'].getboolean('persistent_params', PERSISTENT_PARAMS)


    # Display size
    pars['width'] = config['Display'].getint('width', fallback=1920)
    pars['height'] = config['Display'].getint('height', fallback=1080)
    # Internal buffer
    pars['wd'] = config['Display'].getint('wd', fallback=400)
    pars['ht'] = config['Display'].getint('ht', fallback=300)
    # Font
    pars['font_scale'] = float(config['Font']['scale'])
    pars['font_line'] = int(config['Font']['line'])

    # Content
    if 'Content' in config:
        pars['stream'] = config['Content'].get('video', 'default.mp4')
        pars['images'] = config['Content'].get('images', '')
        pars['texts'] = config['Content'].get('texts', '')
        pars['audio'] = config['Content'].get('audio', '')

    # Triggers
        if 'Triggers' in config:
            jpeg_trigger = Trigger(rule=config['Triggers'].get('images', fallback='timer, 10000'))
            text_trigger = Trigger(rule=config['Triggers'].get('texts', fallback='timer, 4000'))
        elif pars['images'] != '' or pars['texts'] != '':
            print('WARNING: triggers are not defined for images and texts')


def put_text(image, text, scale=0.45, corner=(10, 10), line=1, color=(0, 127, 127)):
    font = cv2.FONT_HERSHEY_SIMPLEX
    corner_of_text = corner
    font_scale = scale
    font_color = color
    line_type = line

    cv2.putText(image, text,
                (corner_of_text[0] + 1, corner_of_text[1] + 1),
                font,
                font_scale,
                (200, 0, 0),
                line_type)
    cv2.putText(image, text,
                corner_of_text,
                font,
                font_scale,
                font_color,
                line_type)
    return cv2.getTextSize(text, font, font_scale, line_type)


def start_writing(width, height, name='output.mp4', rate=20.0):
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    return cv2.VideoWriter(name, fourcc, rate, (width, height))


def open_videos():
    global time_out
    global writing2
    if SONG_MODE:
        cap = cv2.VideoCapture(pars['stream'])
        if cap.isOpened():
            videos.append((cap, 'MP4'))
        else:
            print('Nothing to do for SONG_MODE')
            quit(1)
        return

    for i in range(10):
        cap = cv2.VideoCapture(i)
        if cap.read()[0]:
            videos.append((cap, 'CAM'))
            if TIME_LAPSE and not i:
                ret, frame = cap.read()
                time_out = start_writing(frame.shape[1], frame.shape[0], name='timelapse.mp4', rate=50.0)
                if time_out:
                    writing2 = True
                    break   # only one live camera in TIME_LAPSE mode

    cap = cv2.VideoCapture(pars['stream'])
    if cap.isOpened():
        videos.append((cap, 'MP4'))

    cap = cv2.VideoCapture(pars['images'])
    if cap.isOpened():
        while True:
            ret, frame = cap.read()
            if not ret:
                print('Loaded ' + str(len(jpegs)) + ' jpegs')
                cap.release()
                break
            jpegs.append(frame)
        if len(jpegs) > 0:
            videos.append((None, "JPEG"))

    try:
        text_file = open(pars['texts'], 'r')
        global texts
        texts = text_file.readlines()
        if len(texts) > 0:
            videos.append((None, "TXT"))
    except IOError:
        print('error: ', sys.exc_info()[0])

    print(videos)


def show_next_text(buffer):
    buffer[:, :, :] = 0
    cx = pars['wd'] // 16
    cy = pars['ht'] * 2 // 5
    global text_idx
    text_pair = texts[text_idx].split('|')
    tt = text_pair[0].split('#')
    text_idx += 1
    if text_idx >= len(texts):
        text_idx = 0
    for t in tt:
        sz = put_text(buffer, t.strip(), corner=(cx, cy), scale=pars['font_scale'], line=pars['font_line'], color=(255, 255, 255))
        cy += sz[0][1] + sz[1]
    if len(text_pair) > 1:
        t = text_pair[1]
        put_text(buffer, t.strip(), corner=(cx, cy), scale=pars['font_scale'] * 0.8, line=pars['font_line'], color=(220, 220, 220))


def show_next_jpeg():
    global jpeg_idx
    if jpeg_trigger.is_triggered():
        jpeg_idx += 1
        if jpeg_idx >= len(jpegs):
            jpeg_idx = 0
    return jpegs[jpeg_idx]


def main_loop():
    global frame_time
    fill_screen = True
    global show_info
    global writing
    global text_buffer
    width = pars['width']
    height = pars['height']
    cv2.namedWindow('PyVi', cv2.WND_PROP_FULLSCREEN)  # cv2.WINDOW_NORMAL)
    cv2.setWindowProperty("PyVi", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    result = np.zeros((pars['ht'], pars['wd'], 3), np.uint8)
    if len(texts) > 0:
        text_buffer = np.zeros((pars['ht'], pars['wd'], 3), np.uint8)

    au = AudioPlayer.AudioPlayer(pars['audio'], not SONG_MODE)
    frame_number = 1

    if SONG_MODE:
        out = start_writing(width, height)
        if out:
            writing = True

    while not AudioPlayer.AudioPlayer.get_stopped():
        stream = 0
        for v, p in videos:
            stream += 1
            # Capture frame-by-frame
            if p == 'CAM' or p == 'MP4':
                ret, frame = v.read()
                if ret and p == 'CAM':
                    if writing2 and not (frame_number % TIME_LAPSE):
                        time_out.write(frame)

                    frame = cv2.flip(frame, 1)
            elif p == 'TXT':
                if text_trigger is None:
                    continue
                ret = True
                if text_trigger.is_triggered():
                    show_next_text(text_buffer)
                frame = text_buffer
            elif p == 'JPEG':
                ret = True
                frame = show_next_jpeg()

            if ret:
                frame = Transforms.process_frame(stream, frame, source=p)
            else:
                v.set(cv2.CAP_PROP_POS_FRAMES, 0)
                continue

            alpha = 1 / stream
            result = cv2.addWeighted(result, 1 - alpha, frame, alpha, 0, -1)

        w = cv2.resize(result, (width, height))

        if writing:
            out.write(w)
            put_text(w, 'R')
        elif show_info:
            t = time.time()
            fps = ''
            if frame_time != 0:
                diff = t - frame_time
                if diff != 0:
                    fps = '; ' + str(int(1 / diff)) + ' fps'
            frame_time = t
            put_text(w, INFO_TEXT + fps)
        elif int(time.time()) < message_time:
            put_text(w, message)

        cv2.imshow('PyVi', w)
        c = cv2.waitKey(1) & 0xFF
        if c == ord('q'):
            break
        else:
            if c == ord('?'):
                show_info = not show_info
            elif c == ord('s'):
                cv2.imwrite('shot' + str(frame_number) + '.jpg', w)
            elif c == ord('w'):
                if writing:
                    out.release()
                else:
                    out = start_writing(width, height)
                    if out:
                        writing = False  # will be changed

                writing = not writing

            elif c == ord('W'):
                fill_screen = not fill_screen
                if fill_screen:
                    cv2.namedWindow('PyVi', cv2.WND_PROP_FULLSCREEN)
                    cv2.setWindowProperty("PyVi", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
                else:
                    cv2.namedWindow('PyVi', cv2.WINDOW_NORMAL)
                    cv2.setWindowProperty("PyVi", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL)

            else:
                Transforms.handle_key(c)

        txt = MidiInputs.handle_midi(Transforms.params)
        if txt != '':
            info(txt)
            if 'note' in txt and text_trigger is not None and jpeg_trigger is not None:
                text_trigger.count()
                jpeg_trigger.count()

        frame_number += 1
        Automation.automate()

    if writing2:
        time_out.release()

    for v, p in videos:
        if p == 'CAM' or p == 'MP4':
            v.release()

    au.stop()


if __name__ == "__main__":

    load_config(CONFIG_FILE)
    MidiInputs.load_config(CONFIG_FILE)
    open_videos()
    Transforms.load_config(CONFIG_FILE, videos)
    if PERSISTENT_PARAMS:
        Transforms.load_params(PARAMS_FILE)

    main_loop()

    if PERSISTENT_PARAMS:
        Transforms.save_params(PARAMS_FILE)

    MidiInputs.cleanup()
    cv2.destroyAllWindows()
