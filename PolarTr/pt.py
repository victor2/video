import polarTransform
import imageio
import numpy as np

radius1 = 800
radius2 = radius1 * 2
shift = 0 # radius1 // 2
left = radius2 - radius1 + shift 
right = radius2 + radius1 + shift
arc = 2 * np.pi
arc0 = 0 # np.pi / 2

originalImage = imageio.imread('image.jpg')

centerImage, ptSettings = polarTransform.convertToCartesianImage(originalImage[:, ::-1, :],
                                                                 initialRadius=0,
                                                            finalRadius=radius1, initialAngle=arc0,
                                                            finalAngle=arc0 + arc, hasColor = True)

# centerImage //= 2
imageio.imwrite('result.jpg', centerImage)

#roundImage, ptSettings = polarTransform.convertToCartesianImage(originalImage, initialRadius=radius1 - 50,
#                                                            finalRadius=radius2, initialAngle=0,
#                                                            finalAngle=arc, hasColor = True)

#imageio.imwrite('result2.jpg', roundImage)

#roundImage[left:right, left:right, ] += centerImage

#imageio.imwrite('result3.jpg', roundImage)


# >>> b = b.reshape(3,3)
# >>> a[::, 2:5] = b
