import cv2
import numpy as np
import imageio
import time
import os


def file_name(idx):
    return 'output' + str(idx) + '.mp4'


def output_file():
    idx = 1
    while os.path.isfile(file_name(idx)):
        idx += 1
    return file_name(idx)


def main_loop(angle, scale, rotate, inverse):
    cap = cv2.VideoCapture('video.mp4')
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    # find a good name
    fn = output_file()
    out = cv2.VideoWriter(fn, fourcc, cap.get(cv2.CAP_PROP_FPS), (width, height))

    cy = height // 2
    cx = width // 2
    delta = cx - cy

    cv2.namedWindow('polar', cv2.WINDOW_NORMAL)  # cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("polar", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    matrix = cv2.getRotationMatrix2D((cx, cx), angle, scale)

    started = time.time()
    flags = cv2.WARP_POLAR_LINEAR
    if inverse:
        flags += cv2.WARP_INVERSE_MAP

    cnt = 0
    while True:
        ret, frame = cap.read()
        if not ret:
            cap.release()
            break

        if rotate:
            frame = cv2.transpose(frame)
            if rotate < 0:
                frame = cv2.flip(frame, 1)

        image = cv2.warpPolar(frame, (width, width), (cx, cx), width // 2, flags)
        affine = cv2.warpAffine(image, matrix, (width, width))

        w = affine[delta:height + delta, :, :]
        cv2.imshow('polar', w)
        out.write(w)

        if not cnt:
            print(cx, cy, delta, frame.shape, image.shape, w.shape)
        cnt += 1

        c = cv2.waitKey(1) & 0xFF
        if c == ord('q'):
            break
        elif c == ord('s'):
            cv2.imwrite('frame' + str(cnt) + '.jpg', affine)

    if cnt:
        elapsed = time.time() - started
        print(cnt, ' frames in ', int(elapsed), ' sec. ', cnt / elapsed, ' fps')
    out.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    angle = float(input('Angle '))
    scale = float(input('Scale '))
    rotate = int(input('Rotate [0/1/-1] '))
    main_loop(angle, scale, rotate, True)
    
